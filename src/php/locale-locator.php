<?php
/**
 * Created by PhpStorm.
 * User: Bascoder
 * Date: 7-4-2015
 * Time: 18:58
 */


/**
 * Inserts an Angular locale script based on the users OS/browser language.
 */
function insertLocale()
{
    // don't display errors on the page
    error_reporting(0);

    $locale = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

    $script = 'bower_components/angular-locale/angular-locale_' . $locale . '.js';
    $scriptName = $_SERVER['DOCUMENT_ROOT'] . '/' . $script;
    $exists = file_exists($scriptName);
    if ($exists) {
        $success = printf('<!--suppress HtmlUnknownTarget -->
                <script src="%s"></script>', $script);

        if (!$success) {
            trigger_error("Tried to insert $script into DOM but failed", E_USER_WARNING);
        }
    } else {
        trigger_error('<!--' . $scriptName . ' not found -->', E_USER_WARNING);
    }
}
