(function () {
    "use strict";

    var app = angular.module('portfolio');

    app.directive('dDecodeEmail', function () {

        return function (scope, element, attr) {
            element.val(attr.value['normal']);
        }
    });

})();
