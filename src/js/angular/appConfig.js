(function () {
    "use strict";

    var app = angular.module('portfolio');

    app.config([
        '$compileProvider',
        /**
         * Makes skype URIs accessible
         * @param $compileProvider
         */
            function ($compileProvider) {
            $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|skype):/);
        }]);

    app.config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'pages/home.html'
            })
            .when('/cv', {
                templateUrl: 'pages/cv.html',
                controller: 'CvController',
                controllerAs: 'cvCtrl'
            })
            .when('/projects', {
                templateUrl: 'pages/projects.html',
                controller: 'ProjectController',
                controllerAs: 'projectCtrl'
            })
            .when('/education', {
                templateUrl: 'pages/education.html'
            })
    });
})();
