(function () {
  "use strict";

  var app = angular.module('portfolio');
  var SMALL_BREAKPOINT = 768;

  function ScreenUtilService($window) {
    /**
     * Returns true if screen is small
     * @returns {bool}
     */
    this.isSmall = function () {
      // matchMedia isn't supported by all 'modern' browsers yet
      if (angular.isDefined($window['matchMedia'])) {
        var mq = $window.matchMedia("(max-width: " + SMALL_BREAKPOINT + "px)");
        return mq.matches;
      } else {
        return $window.innerWidth <= SMALL_BREAKPOINT;
      }
    }
  }

  app.service('ScreenUtilService', ['$window', ScreenUtilService]);
})();
