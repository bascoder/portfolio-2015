(function () {
    "use strict";

    var app = angular.module('portfolio');

    app.controller('ProjectController', function () {
        // sort projects by start date in descending order
        this.projects = projects.sort(function (a, b) {
            if (a.dateStart > b.dateStart) return -1;
            if (a.dateStart < b.dateStart) return 1;

            return 0;
        });
    });
})();
