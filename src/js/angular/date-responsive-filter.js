(function () {
  var app = angular.module('portfolio');

  /**
   * Filter makes dates display smaller on small devices
   */
  app.filter('dateResponsive', ['$filter', 'ScreenUtilService',
    function ($filter, ScreenUtilService) {
      return function (date, timezone) {
        var format;
        if (ScreenUtilService.isSmall()) {
          format = 'yyyy';
        } else {
          format = 'yyyy, MMMM';
        }
        return $filter('date')(date, format, timezone);
      }
    }]);
})();
