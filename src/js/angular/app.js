(function () {
  "use strict";

  // create portfolio module
  var app = angular.module('portfolio', ['ngRoute', 'ngSanitize', 'ngAnimate', 'angular-repeat-n']);

  /**
   * Config constant contains pages
   */
  app.constant('config', {
    pages: [
      {
        id: 0,
        name: 'Home',
        route: '/'
      },
      {
        id: 1,
        name: 'About Me',
        route: '/cv'
      }, {
        id: 2,
        name: 'Projects',
        route: '/projects'
      },
      {
        id: 3,
        name: 'Education',
        route: '/education'
      }
    ]
  });

})();
