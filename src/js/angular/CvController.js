(function () {
    "use strict";

    var app = angular.module('portfolio');

    app.controller('CvController', ['$scope', '$http', function ($scope, $http) {
        this.cv = cvInfo;
        this.portrait = {
            url: 'img/portrait-me-small.png',
            alt: 'Portrait of me'
        };

        this.schools = {
            array: education,
            title: 'Education'
        };
        this.work = {
            array: work,
            title: 'Work Experience'
        };
        this.courses = {
            array: courses,
            title: 'Additional Certificates'
        };
        this.languages = {
            array: languages,
            title: 'Languages'
        };
        this.itSkills = itSkills;

        $scope.summaryHtml = '';

        $http.get('data/summary-cv.html').then(function (response) {
            $scope.summaryHtml = response.data;
        });
    }]);
})();
