(function () {
    "use strict";

    var app = angular.module('portfolio');

    app.controller('TabController',
        ['$scope', '$log', '$location', 'config', function ($scope, $log, $location, config) {

            this.tabs = config.pages;

            this.setSelected = function (newValue) {
                $location.path(newValue.route);
            };

            this.isSelected = function (tab) {
                return $location.path() == tab.route;
            };

            $log.debug('scope is loaded');
            $scope.loaded = true;
        }]);
})();
