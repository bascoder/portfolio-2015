var lorumIpsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum lorem non efficitur pellentesque. Nullam vehicula dolor vitae sem mattis dapibus. Morbi vitae tortor vel ex laoreet molestie in vitae ex. Suspendisse enim mauris, pretium non ultricies eget, cursus nec sem. Etiam maximus eget mi id gravida. Duis at mi at diam vehicula auctor. Vestibulum eu condimentum purus, eget posuere tortor. Vivamus eu erat augue. Nulla vitae nunc ante. Aenean placerat sem sem, vel vehicula nibh vehicula quis.';

var cvInfo = {
    firstName: 'Bas',
    lastName: 'van Marwijk',
    dateOfBirth: new Date(1994, 9, 21),
    placeOfBirth: 'Leidschendam, Netherlands',
    nationality: 'Dutch',
    emailAddress: {
        obscured: 'moc.liamg@kjiwram.nav.sab'
    },
    skypeId: {
        id: 'basvanmarwijk',
        uri: 'skype:basvanmarwijk?chat',
        webUrl: ''
    },
    gitHubProfile: {
        id: 'bascoder',
        uri: 'https://github.com/bascoder'
    },
    bitbucketProfile: {
        id: 'bascoder',
        uri: 'https://bitbucket.org/bascoder/'
    }
};

var courses = [
    {
        date: new Date(2015, 3),
        title: 'Web Application Architectures',
        institution: 'Coursera (University of New Mexico)',
        url: 'https://www.coursera.org/account/accomplishments/verify/WDFBCYQYAR'
    },
    {
        date: new Date(2014, 9),
        title: 'Programming Cloud Services for Android Handheld Systems',
        institution: 'Coursera (Vanderbilt University)',
        url: 'https://www.coursera.org/account/accomplishments/records/bSrPpwb6C6LKzUjk'
    },
    {
        date: new Date(2014, 3),
        title: 'Programming Mobile Applications for Android Handheld Systems',
        institution: 'Coursera (University Of Maryland)',
        url: 'https://www.coursera.org/account/accomplishments/records/bbGMHePWuYrG78KC'
    }
];

var work = [
    {
        date: '2016, February - now',
        dateStart: new Date(2016, 1),
        dateEnd: 'now',
        company: '',
        jobPosition: 'Freelance Software Developer',
        place: 'The Hague Area'
    },
    {
        date: '2015, July - 2016, February',
        dateStart: new Date(2015, 6),
        dateEnd: new Date(2016, 1),
        company: 'Transputec Ltd',
        jobPosition: 'Part Time Software Developer',
        place: 'Remote'
    },
    {
        date: '2015, February - 2015, July',
        dateStart: new Date(2015, 1),
        dateEnd: new Date(2015, 6),
        company: 'Transputec Ltd',
        jobPosition: 'Intern Developer',
        place: 'London, United Kingdom'
    },
    {
        date: '2011, September - 2015, February',
        dateStart: new Date(2011, 8),
        dateEnd: new Date(2015, 1),
        company: 'Albert Heijn',
        jobPosition: 'Supermarket worker',
        place: 'Leidschendam, The Netherlands'
    },
    {
        date: '2013, September - 2014, October',
        dateStart: new Date(2013, 8),
        dateEnd: new Date(2014, 9),
        company: 'Studentaanhuis (Student@Home)',
        jobPosition: 'Computer Doctor',
        place: 'Leidschendam, The Netherlands'
    },
    {
        date: "2008 - 2011",
        dateStart: new Date(2008, 6),
        dateEnd: new Date(2011, 2),
        company: 'Network VSP',
        jobPosition: 'News boy',
        place: 'Leidschendam, The Netherlands'
    }
];

var education = [
    {
        date: "2013, September - now",
        dateStart: new Date(2013, 8),
        dateEnd: 'now',
        study: "Computer Science",
        school: "The Hague University Of Applied Sciences",
        place: "The Hague"
    },
    {
        date: "2012, September - 2013, September",
        dateStart: new Date(2012, 8),
        dateEnd: new Date(2013, 8),
        study: "Computer Engineering",
        school: "The Hague University Of Applied Sciences",
        place: "Delft",
        addition: "not completed"
    },
    {
        date: "2006 - 2012",
        dateStart: new Date(2006, 8),
        dateEnd: new Date(2012, 5),
        study: "HAVO, Nature, Technology and Economics",
        school: "St. Maartens College Voorburg",
        place: "Voorburg",
        addition: "high school"
    }
];

var projects = [
    {
        title: 'Maze Game',
        caseDescription: 'As part of the computer science major at the Hague University me and another student developed a 2D maze game.' +
        'The goal of this project was to practice object oriented programming in Java and managing changing requirements during the project using SCRUM.',
        date: 'October 2013 - February 2014',
        dateStart: new Date(2013, 9),
        dateEnd: new Date(2014, 1),
        mainImage: {
            url: 'img/example-full.jpg',
            thumbnail: 'img/example-thumbnail.jpg',
            alt: 'Screen shot of the Maze Game'
        },
        imageGallery: [],
        source: {
            url: '',
            description: 'N/A',
            show: false
        }
    },
    {
        title: 'Database merge',
        caseDescription: 'As part of the computer science major at the Hague University me and 3 other students did a project to practice SQL Server skills.' +
        'The case of the project was that two fictional companies were going to merge and they needed our help to merge their information systems and add functionality for their new business goals.<br />' +
        'The companies sell holiday and free time equipment to customers, the first company sells mainly to resellers and the second company sells to private customers.' +
        ' Also, as new functionality customers can book packed holidays.' +
        'As part of the project we analysed data in the current databases of the two company and we created a design for the new database.' +
        'Also we created a merge plan so that existing data can be transferred smoothly to the new database.',
        date: 'April 2014 - June 2014',
        dateStart: new Date(2014, 3),
        dateEnd: new Date(2014, 5),
        mainImage: {
            url: 'img/example-full.jpg',
            thumbnail: 'img/example-thumbnail.jpg',
            alt: 'Screenshot database merge'
        },
        imageGallery: [],
        source: {
            url: '',
            description: 'N/A',
            show: false
        }
    },
    {
        title: '2D adventure/RPG: Mr Meteor and his quest of planetary conquest',
        caseDescription: 'This project was about creating a 2D game for the kb24 minor at the Hague University. Me an three other students developed the game using the XNA framework with C# .NET. ' +
        'As software development methodology we used eXtreme Programming and TDD. This is the perfect methodology for game development since requirements change on daily basis.' +
        ' Using a large set of unit tests we made sure the code still works after refactoring code or adding complex logic.',
        date: 'October 2014 - February 2015',
        dateStart: new Date(2014, 9),
        dateEnd: new Date(2015, 1),
        mainImage: {
            url: 'img/project_mrmeteor.png',
            thumbnail: 'img/project_mrmeteor-small.png',
            alt: 'Screenshot Mr Meteor game'
        },
        imageGallery: [],
        source: {
            url: '',
            description: 'N/A',
            show: false
        }
    },
    {
        title: 'My Locations Android App',
        caseDescription: '<p>As part of the Android MOOC I followed during early 2014 had to develop a small app to track locations that the user visited.<br />' +
        'I continued developing this app with additional functionality like storing photos<br />' +
        'The main goal of this project is to practice my Android development skills and explore the Android APIs.',
        date: 'March 2014 - now',
        dateStart: new Date(2014, 2),
        dateEnd: 'now',
        mainImage: {
            url: 'img/project_mylocations.png',
            thumbnail: 'img/project_mylocations-small.png',
            alt: 'Screenshot of My Locations app'
        },
        source: {
            url: 'https://github.com/bascoder/mylocations',
            description: 'Github',
            show: true
        }
    }
];

var itSkills = {
    general: [{
        title: 'Windows',
        stars: 5,
        weight: 5,
        description: 'Windows is my primary Operating System. As part of my job at <a href="http://studentaanhuis.nl">StudentAanHuis</a> I did PC maintenance at customer site I\'ve done a lot of troubleshooting with Windows computers.'
    }, {
        title: 'Office: Word/ Excel/ Powerpoint/ Outlook',
        stars: 5,
        weight: 4,
        description: ''
    }, {
        title: 'Linux',
        stars: 3,
        weight: 3,
        description: 'On my desktop PC I\'m using Linux Mint as second operating system. Also I\'ve got a Raspberry Pi which requires its fair share of Linux experience.<br /> During my course at Computer Engineering at The Hague University we were taught a class in Linux Command Line Interfaces.'
    }, {
        title: 'PC maintenance',
        stars: 5,
        weight: 2,
        description: 'As part of my job at <a href="http://studentaanhuis.nl">StudentAanHuis</a> I did PC maintenance at customer site. Customers were mainly regular consumers and small businesses.'
    }],
    'programming languages': [{
        title: 'Java',
        stars: 5,
        weight: 8,
        description: 'As part of my education at the Hague University I was taught a lot of Java.<br />Also it\'s the primary language used in Android projects'
    }, {
        title: 'C# .NET',
        stars: 5,
        weight: 6,
        description: 'As web developer at Transputec I\'ve used the ASP.NET framework as backend framework for some projects. I also have experience with the XNA framework which uses C# as primary language.'
    }, {
        title: 'JavaScript',
        stars: 5,
        weight: 7,
        description: 'As web developer at Transputec I\'ve done a lot of front-end development with JavaScript in jQuery and AngularJS. Also I\'ve been playing around with back-end environments like NodeJS.'
    }, {
        title: 'HTML5',
        stars: 5,
        weight: 5,
        description: 'As web developer I\'ve had a lot of experience with HTML interfaces. I like building modern web interfaces using new features of HTML5.'
    }, {
        title: 'SASS/ CSS',
        stars: 3,
        weight: 4,
        description: 'As part of developing web interfaces I\'ve gained some experience with CSS. To optimise productivity, and minimize code duplication I prefer using SASS as preprocessor.'
    }, {
        title: 'SQL',
        stars: 4,
        weight: 3,
        description: 'For relational databases SQL is the default language of choice. I have experience with several DBMSes like: SQL Server, MySQL, PostgreSQL and SQLite'
    }, {
        title: 'PHP',
        stars: 4,
        weight: 2,
        description: 'PHP is the first language I used to develop dynamic websites. Also I\'ve used it in several projects during my internship at Transputec.'
    }, {
        title: 'C/C++',
        stars: 2,
        weight: 1,
        description: 'As part of Computer Engineering at The Hague University I\'ve gained some experience with C and C++. These languages helped me to understand low level programming concepts like dynamic memory allocation, memory leaks, pointer references, etc.'
    }],
    'technologies': [{
        title: 'Android Mobile Development',
        stars: 4,
        weight: 10,
        description: 'I completed three MOOCs about Android Development. Also I worked on several Android projects.'
    }, {
        title: 'XNA 2D games',
        stars: 4,
        weight: 7,
        description: 'At the Hague University I followed a course in XNA programming. With 4 project members we have developed a 2D game as part of the course.'
    }, {
        title: 'Web services/ Web APIs',
        stars: 4,
        weight: 8,
        description: 'Using ASP.NET Web API 2.0 I have experience developing Restfull web APIs.'
    }, {
        title: 'SCRUM',
        stars: 3,
        weight: 5,
        description: 'During my course at The Hague University they taught us several Agile software development methodologies like SCRUM, XP and UP.'
    }, {
        title: 'AngularJS',
        stars: 4,
        weight: 9,
        description: 'As a backend developer AngularJS is a great JavaScript framework for frontend development. I\'ve used AngularJS in several projects for Transputec.'
    }, {
        title: 'Git',
        stars: 4,
        weight: 6,
        description: 'See my <a href="https://github.com/bascoder">GitHub</a> and <a href="https://bitbucket.org/bascoder/">BitBucket</a> profiles.'
    }, {
        title: 'Unit Testing',
        stars: 4,
        weight: 4,
        description: 'As part of my education I was taught testing. Also unit testing using JUnit for Java, or NUnit for C#, and Jasmine/Karma for AngularJS.'
    },{
        title: 'SQL Server',
        stars: 5,
        weight: 5,
        description: 'As part of my study at The Hague University I\'ve participated in a project where we did a database migration between one SQL Server database, one MySQL database, one Access Database, and one Excel spreadsheet. Finally all data was migrated to one SQL Server database.'
    }, {
        title: 'MySQL',
        stars: 4,
        weight: 5,
        description: 'MySQL was the first DBMS I used for database development'
    }, {
        title: 'PostgreSQL',
        stars: 3,
        weight: 4,
        description: 'During my internship at Transputec I worked on a project which makes use of a PostgreSQL database. Therefor I studied their documentation.'
    }]
};

var languages = [
    {
        language: 'Dutch',
        level: 'Mother tongue'
    },
    {
        language: 'English',
        level: 'Good'
    },
    {
        language: 'Spanish',
        level: 'Beginner'
    }
];
