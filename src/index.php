<?php include_once('php/locale-locator.php'); ?>
<!DOCTYPE html>
<html ng-app="portfolio">
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Bas van Marwijk's portfolio">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Portfolio Bas van Marwijk</title>

    <link href="css/main.css?v=6" rel="stylesheet" type="text/css"/>

    <link rel="icon" href="favicon.ico" type="image/x-icon" />
</head>
<body>
<!--[if lt IE 9]>
<?php include_once('templates/browser-upgrade-notice.html') ?>
<![endif]-->
<noscript>
    <p class="browser-upgrade">
        Please enable Javascript to view my portfolio.
    </p>
</noscript>
<header>
    <div ng-include="'templates/tabs.html'"></div>
</header>

<div class="container">
    <div class="row container-content ng-cloak" ng-cloak>
        <div ng-view>

        </div>
    </div>
</div>
<!--[if gte IE 9]><!-->
<!-- AngularJS Libraries -->
<script src="bower_components/angular/angular.min.js"></script>
<script src="bower_components/angular-route/angular-route.min.js"></script>
<script src="bower_components/angular-sanitize/angular-sanitize.min.js"></script>
<script src="bower_components/angular-animate/angular-animate.min.js"></script>
<script src="bower_components/angular-repeat-n/dist/angular-repeat-n.js"></script>

<!-- JS objects -->
<script src="js/data.js"></script>

<!-- Angular app -->
<script src="js/angular/app.js?v=4"></script>
<script src="js/angular/appConfig.js"></script>
<script src="js/angular/ScreenUtilService.js"></script>
<script src="js/angular/CvController.js"></script>
<script src="js/angular/ProjectController.js"></script>
<script src="js/angular/TabController.js"></script>
<script src="js/angular/DecodeEmailDirective.js"></script>
<script src="js/angular/date-responsive-filter.js"></script>

<!-- Insert AngularJS locale file -->
<?php insertLocale(); ?>

<!-- Not critical libraries -->
<script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
<!--<![endif]-->
<!-- Google analytics -->
<script src="js/analytics.js"></script>
</body>
</html>
